<?php

namespace KrrAcfCategory;

/**
 * Class Plugin
 *
 * @package KrrAcfCateg
 */
class Plugin
{
    /* @var Plugin|null */
    public static ?Plugin $instance = null;

    /**
     * Plugin constructor.
     */
    public function __construct()
    {

        $dependencies = Dependencies::getInstance();
        $dependencies->check();

        // Continue only if no dependencies are missing
        if (count($dependencies->missingDependencies) === 0) {
            add_action('plugins_loaded', [$this, 'init'], 1);
            add_action('admin_enqueue_scripts', [$this, 'enqueueScripts']);
            add_action('admin_menu', [$this, 'addMenuPage'], 15);
        }
    }

    /**
     * Get the instance of the current class
     *
     * @return ?Plugin
     */
    public static function getInstance(): ?Plugin
    {
        if (self::$instance === null) {
            self::$instance = new Plugin();
        }

        return self::$instance;
    }

    /**
     * Initialisation of the plugin
     */
    public function init(): void
    {
        // Load i18n translations
        load_plugin_textdomain(
            'krr-acfcat',
            false,
            dirname(plugin_basename(KRR_ACFCAT_FILE)) . '/languages'
        );

        // Init the taxonomy declaration et the admin view
        new Taxonomy();
        new AdminView();
    }

    /**
     * Add scripts
     *
     * @return void
     */
    public function enqueueScripts(): void
    {
        wp_enqueue_style('krr-acfcat', plugin_dir_url(KRR_ACFCAT_FILE) . 'assets/css/acf-category.css');
    }

    /**
     * Add the category menu entry
     */
    public function addMenuPage(): void
    {
        add_submenu_page(
            'edit.php?post_type=acf-field-group',
            __('Categories', 'krr-acfcat'),
            __('Categories', 'krr-acfcat'),
            'manage_options',
            'edit-tags.php?post_type=acf-field-group&taxonomy=' . Taxonomy::SLUG,
            '',
            5
        );
    }
}