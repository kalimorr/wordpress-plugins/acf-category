<?php

namespace KrrAcfCategory;

/**
 * Class Taxonomy
 *
 * @package KrrAcfCategory
 */
class Taxonomy
{
    /* @var string Name of the selector field in edit page */
    const SLUG = 'acf-category';
    const PARENT_SLUG = 'acf-field-group';

    /**
     * Plugin constructor.
     */
    public function __construct()
    {
        add_action('init', [$this, 'registerTaxonomy'], 0);
    }

    /**
     * Register the taxonomy
     */
    public function registerTaxonomy(): void
    {
        $labels = [
            'name'          => _x('ACF Categories', 'Taxonomy General Name', 'krr-acfcat'),
            'singular_name' => _x('ACF Category', 'Taxonomy Singular Name', 'krr-acfcat'),
            'menu_name'     => __('Category', 'krr-acfcat'),
        ];

        $args = [
            'labels'             => $labels,
            'hierarchical'       => true,
            'public'             => true,
            'show_ui'            => true,
            'show_admin_column'  => false,
            'show_in_nav_menus'  => false,
            'show_tagcloud'      => false,
            'publicly_queryable' => false,
            'rewrite'            => false
        ];

        register_taxonomy(self::SLUG, self::PARENT_SLUG, $args);
    }
}