<?php

namespace KrrAcfCategory;

class AdminView
{
    public function __construct()
    {
        add_filter('manage_acf-field-group_posts_columns', [$this, 'tableColumnEdit'], 11);
        add_action('manage_acf-field-group_posts_custom_column', [$this, 'tableColumnRender'], 10, 2);
        add_action('restrict_manage_posts', [$this, 'dropdownFilter']);
        add_filter('parse_query', [$this, 'editFilteredQuery']);
    }

    /**
     * Customizes the admin table columns.
     *
     * @param array $columns
     *
     * @return array
     */
    public function tableColumnEdit($columns): array
    {
        $columns['acf_category'] = __('Category', 'krr-acfcat');

        return $columns;
    }

    /**
     * Renders the admin table column HTML
     *
     * @param string $column The name of the column to display.
     * @param int    $postId The current post ID.
     *
     * @return  void
     */
    public function tableColumnRender(string $column, int $postId): void
    {
        if ($column === 'acf_category') {
            $terms = get_the_terms($postId, Taxonomy::SLUG);

            if (is_array($terms)) {
                $output = '';

                /* @type \WP_Term $term */
                foreach ($terms as $term) {
                    $output .= '<a href="' . $this->getFilterdTableViewUrl($term->slug) . '">';
                    $output .= $term->name;
                    $output .= '</a>, ';
                }

                echo trim($output, ', ');
            }
        }
    }

    /**
     * Display a custom taxonomy dropdown in admin
     */
    public function dropdownFilter(): void
    {
        global $typenow;

        $postType = Taxonomy::PARENT_SLUG;
        $taxonomy = Taxonomy::SLUG;

        if ($typenow === $postType) {
            $selected     = $_GET[$taxonomy] ?? '';
            $taxonomyData = get_taxonomy($taxonomy);


            wp_dropdown_categories(
                [
                    'show_option_all' => sprintf(__('Show all %s', 'krr-acfcat'), strtolower($taxonomyData->label)),
                    'taxonomy'        => $taxonomy,
                    'name'            => $taxonomy,
                    'orderby'         => 'name',
                    'selected'        => $selected,
                    'show_count'      => true,
                    'hide_empty'      => true,
                ]
            );
        };
    }

    /**
     * Filter acf field groups by taxonomy in admin
     *
     * @param \WP_Query $query
     *
     * @return void
     */
    public function editFilteredQuery(\WP_Query $query): void
    {
        global $pagenow;
        $postType = Taxonomy::PARENT_SLUG;
        $taxonomy = Taxonomy::SLUG;

        $queryVars = &$query->query_vars;

        if ($pagenow == 'edit.php' && isset($queryVars['post_type']) && $queryVars['post_type'] === $postType && isset($queryVars[$taxonomy]) && $queryVars[$taxonomy] !== '0') {
            $term                 = get_term_by('id', $queryVars[$taxonomy], $taxonomy);
            $queryVars[$taxonomy] = $term->slug;
        }
    }

    /**
     * Retrieve the URL of the list of Acf field groups of a term
     *
     * @param string $term Slug of the searched term
     *
     * @return string
     */
    private function getFilterdTableViewUrl(string $term): string
    {
        return admin_url('edit.php?' . Taxonomy::SLUG . '=' . $term . '&post_type=' . Taxonomy::PARENT_SLUG);
    }
}