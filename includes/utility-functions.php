<?php

namespace KrrAcfCategory;

/**
 * Returns the plugin path to a specified file.
 *
 * @param string $filename
 *
 * @return string
 */
function krr_get_path(string $filename = ''): string
{
    return plugin_dir_path(KRR_ACFCAT_FILE) . ltrim($filename, '/');
}

/**
 * Requires a file within a Kofinorr plugin.
 *
 * @param string $filename
 */
function krr_require(string $filename = ''): void
{
    $file_path = krr_get_path($filename);
    if (file_exists($file_path)) {
        require_once($file_path);
    }
}