# ACF Category
You are using Advanced Custom Fields but you are lost among all your field groups and you can not find quickly the one you want to update ? This plugin will allow you to sort your field groups by categories and you will have only the field groups of a selected category that will be shown.

## Requirements
* Require WordPress 6.0+ / Tested up to 6.3.1
* Require PHP 8.0+
* Advanced Custom Fields 6.0.0+

## Installation

### Manual

Download and install the plugin using the built-in WordPress plugin installer.
No settings necessary, it's a plug-and-play plugin !


### Composer

Add the following repository source :

    {
        "type": "vcs",
        "url": "https://gitlab.com/kalimorr/wordpress-plugins/acf-category.git"
    }

Include `"kalimorr/acf-category": "dev-master"` in your composer file for last master's commits or a tag released.  
No settings necessary, it's a plug-and-play plugin !

## How to use
It is really simple. This plugin uses the taxonomy system.

To create a custom term, you just have to add it in the `Categories` submenu of ACF.

Then, when you are on your list of ACF field groups, you can filter them by choosing one of the available terms of the table. 

## Internationalisation
- English (default)
- Français

## License
"ACF Category" is licensed under the GPLv3 or later.
