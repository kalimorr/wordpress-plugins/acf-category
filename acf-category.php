<?php
/**
 * Plugin Name: ACF Category
 * Description: The ideal plugin to find your way easily among your ACF field groups.
 * Version: 1.0.0
 * Author: Kalimorr
 * Author URI: https://gitlab.com/kalimorr
 * Text Domain: krr-acfcat
 * Domain Path: /languages
 * License: GPLv3 or later
 */

use \KrrAcfCategory\Plugin;
use function KrrAcfCategory\krr_require;

/* Security */
defined('ABSPATH')
or die ('no');

const KRR_ACFCAT_FILE = __FILE__;

require_once plugin_dir_path(KRR_ACFCAT_FILE) . 'includes/utility-functions.php';

/* Cal the files */
krr_require('Classes/Dependencies.php');
krr_require('Classes/Taxonomy.php');
krr_require('Classes/AdminView.php');
krr_require('Classes/Plugin.php');

Plugin::getInstance();
