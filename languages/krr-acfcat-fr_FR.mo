��          t       �       �      �   
   �      �      �      �   $     #   (  �   L  D   �       �  2     �     �  
   �     �               %  �   4  U   �        ACF Category Categories Category Kalimorr Show all %s Taxonomy General NameACF Categories Taxonomy Singular NameACF Category The ACF Category plugin cannot be activated because the following required plugins are not active: %s. Please activate these plugins The ideal plugin to find your way easily among your ACF groupfields. https://gitlab.com/kalimorr Project-Id-Version: ACF Category
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2023-09-18 20:36+0000
PO-Revision-Date: 2023-09-18 20:39+0000
Last-Translator: 
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.6; wp-6.3.1
X-Domain: krr-acfcat ACF Category Catégories Catégorie Kalimorr Voir toutes les %s Catégories ACF Catégorie ACF Le plugin ACF Category n'a pas pu être activé car les plugins requis suivants ne sont pas actifs : %s. Merci d'activer ces plugins. Le plugin idéal pour s'y retrouver facilement parmi tous les groupes de champs ACF ! https://gitlab.com/kalimorr 